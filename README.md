# Model documentation

## Auryn Simulator for recurrent spiking neural networks with synaptic plasticity

https://github.com/HBPNeurorobotics/auryn

## Flex sensor

[https://bitbucket.org/alex_vds_ugent/flexsensor](https://bitbucket.org/alex_vds_ugent/flexsensor/src/55bea51a5f24b7e91fec816035566db3d4f7d18e/doc/?at=master)